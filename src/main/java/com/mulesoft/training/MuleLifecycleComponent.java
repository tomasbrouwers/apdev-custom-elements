package com.mulesoft.training;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleException;
import org.mule.api.lifecycle.Callable;
import org.mule.api.lifecycle.Startable;

public class MuleLifecycleComponent implements Callable, Startable {

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		logger.info("Component is executed");
		return eventContext.getMessage();
	}

	@Override
	public void start() throws MuleException {
		logger.info("Component initiated startup!");
	}

	private static final Logger logger = LogManager.getLogger("com.mulesoft.training.Logger");
}
